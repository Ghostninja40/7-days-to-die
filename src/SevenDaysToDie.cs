using System;
using System.Linq;
using uMod.Common;
using uMod.Configuration;
using uMod.Plugins;
using uMod.Plugins.Decorators;

namespace uMod.Game.SevenDaysToDie
{
    /// <summary>
    /// The core 7 Days to Die plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class SevenDaysToDie : Plugin
    {
        #region Initialization

        internal static readonly SevenDaysToDieProvider Universal = SevenDaysToDieProvider.Instance;

        /// <summary>
        /// Initializes a new instance of the SevenDaysToDie class
        /// </summary>
        public SevenDaysToDie()
        {
            // Set plugin info attributes
            Title = "7 Days to Die";
            Author = SevenDaysExtension.AssemblyAuthors;
            Version = SevenDaysExtension.AssemblyVersion;
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this);

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();
        }

        #endregion Initialization

        #region Server Hooks

        /// <summary>
        /// Called when a console command was run
        /// </summary>
        /// <param name="senderInfo"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [Hook("IOnServerCommand")]
        private object IOnServerCommand(CommandSenderInfo senderInfo, string command)
        {
            if (!string.IsNullOrEmpty(command) && command.Trim().Length != 0)
            {
                // Parse the command and args
                string[] fullCommand = CommandLine.Split(command);
                if (fullCommand.Length >= 1)
                {
                    string cmd = fullCommand[0].ToLower();
                    string[] args = fullCommand.Skip(1).ToArray();

                    // Check if sender is local
                    if (senderInfo.IsLocalGame)
                    {
                        // Is the server command blocked?
                        if (Interface.CallHook("OnServerCommand", cmd, args) != null)
                        {
                            return true;
                        }
                    }
                    // Check if sender is remote
                    else if (senderInfo.RemoteClientInfo != null)
                    {
                        // Is the player command blocked?
                        if (Interface.CallHook("OnPlayerCommand", senderInfo.RemoteClientInfo.IPlayer, cmd, args) != null)
                        {
                            return true;
                        }
                    }
                }
            }

            return null;
        }

        #endregion Server Hooks

        #region Player Hooks

        /// <summary>
        /// Called when the player is attempting to connect
        /// </summary>
        /// <param name="clientInfo"></param>
        /// <returns></returns>
        [Hook("IOnPlayerApprove")]
        private object IOnPlayerApprove(ClientInfo clientInfo)
        {
            Players.PlayerJoin(clientInfo.InternalId.ReadablePlatformUserIdentifier, clientInfo.playerName);

            // Call pre hook for plugins
            object canLogin = Interface.CallHook("CanPlayerLogin", clientInfo.playerName, clientInfo.InternalId.ReadablePlatformUserIdentifier, clientInfo.ip);

            // Can the player log in?
            if (canLogin is string || canLogin is bool loginBlocked && !loginBlocked)
            {
                // Reject player with message
                string reason = canLogin is string ? canLogin.ToString() : "Connection was rejected"; // TODO: Localization
                GameUtils.KickPlayerData kickData = new GameUtils.KickPlayerData(GameUtils.EKickReason.ManualKick, 0, default, reason);
                GameUtils.KickPlayerForClientInfo(clientInfo, kickData);
                return true;
            }

            // Call post hook for plugins
            Interface.CallHook("OnPlayerApproved", clientInfo.playerName, clientInfo.InternalId.ReadablePlatformUserIdentifier, clientInfo.ip);
            Interface.CallDeprecatedHook("OnUserApproved", "OnPlayerApproved", new DateTime(2022, 1, 1), clientInfo.playerName, clientInfo.InternalId.ReadablePlatformUserIdentifier, clientInfo.ip);

            return null;
        }

        /// <summary>
        /// Called when the player sends a message
        /// </summary>
        /// <param name="clientInfo"></param>
        /// <param name="message"></param>
        [Hook("IOnPlayerChat")]
        private object IOnPlayerChat(ClientInfo clientInfo, string message)
        {
            if (clientInfo != null && !string.IsNullOrEmpty(message))
            {
                // Is the message a chat command?
                if (Universal.CommandSystem.HandleChatMessage(clientInfo.IPlayer, message) == CommandState.Completed)
                {
                    return true;
                }

                // Is the chat message blocked?
                object chatUniversal = Interface.CallHook("OnPlayerChat", clientInfo.IPlayer, message);
                object chatDeprecated = Interface.CallDeprecatedHook("OnUserChat", "OnPlayerChat", new DateTime(2022, 1, 1), clientInfo.IPlayer, message);
                return chatUniversal ?? chatDeprecated;
            }

            return null;
        }

        /// <summary>
        /// Called when the player has connected
        /// </summary>
        /// <param name="clientInfo"></param>
        [Hook("OnPlayerConnected")]
        private void OnPlayerConnected(ClientInfo clientInfo)
        {
            // Add player to default groups
            GroupProvider defaultGroups = Interface.uMod.Auth.Configuration.Groups;
            if (!permission.UserHasGroup(clientInfo.InternalId.ReadablePlatformUserIdentifier, defaultGroups.Players))
            {
                permission.AddUserGroup(clientInfo.InternalId.ReadablePlatformUserIdentifier, defaultGroups.Players);
            }
            if (GameManager.Instance.adminTools.IsAdmin(clientInfo) && !permission.UserHasGroup(clientInfo.InternalId.ReadablePlatformUserIdentifier, defaultGroups.Administrators))
            {
                permission.AddUserGroup(clientInfo.InternalId.ReadablePlatformUserIdentifier, defaultGroups.Administrators);
            }

            Players.PlayerConnected(clientInfo.InternalId.ReadablePlatformUserIdentifier, clientInfo);
        }

        /// <summary>
        /// Called when the player has disconnected
        /// </summary>
        /// <param name="clientInfo"></param>
        [Hook("OnPlayerDisconnected")]
        private void OnPlayerDisconnected(ClientInfo clientInfo)
        {
            Players.PlayerDisconnected(clientInfo.InternalId.ReadablePlatformUserIdentifier);
        }

        #endregion Player Hooks
    }
}
