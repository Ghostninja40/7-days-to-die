﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDie.Init")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDie.IOnPlayerChat(ClientInfo,System.String)~System.Object")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDie.IOnServerCommand(CommandSenderInfo,System.String)~System.Object")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDie.IOnServerInitialized")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDie.IOnUserApprove(ClientInfo)~System.Object")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDie.OnPlayerConnected(ClientInfo)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDie.OnPlayerDisconnected(ClientInfo)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDie.OnPluginLoaded(uMod.Plugins.Plugin)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDie.OnServerInitialized")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDie.OnServerSave")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDie.OnServerShutdown")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDieConsolePlayer.Teleport(System.Single,System.Single,System.Single)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDieConsolePlayer.Heal(System.Single)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDieConsolePlayer.Kick(System.String)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDieConsolePlayer.Ban(System.String,System.TimeSpan)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDieConsolePlayer.Hurt(System.Single)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDieConsolePlayer.Rename(System.String)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDieConsolePlayer.Respawn(uMod.Common.Position)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.SevenDaysToDie.SevenDaysToDieConsolePlayer.Teleport(uMod.Common.Position)")]
